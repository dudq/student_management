package com.formatter;

import com.models.Mark;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class MarkValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Mark.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "dateOfExamination", "date.empty", "Invalid date");
    }
}
