package com.controllers;

import com.models.Subject;
import com.services.MarkService;
import com.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SubjectController {
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private MarkService markService;

    @GetMapping("/admin/subjects")
    public String subjectList(Model model, @PageableDefault(size = 10, sort = "name") Pageable pageable) {
        Page<Subject> subjects = subjectService.findAll(pageable);
        model.addAttribute("subjects", subjects);
        return "subject/list";
    }

    @GetMapping("/admin/create-subject")
    public String createSubject(Model model) {
        model.addAttribute("subject", new Subject());
        return "subject/create";
    }

    @PostMapping("/admin/create-subject")
    public String saveNewSubject(Subject subject, Model model) {
        subjectService.save(subject);
        model.addAttribute("message", "Added new subject");
        model.addAttribute("subject", new Subject());
        return "subject/create";
    }

    @GetMapping("/admin/subject/edit/{id}")
    public String editSubject(@PathVariable Long id, Model model) {
        Subject subject = subjectService.findById(id);
        model.addAttribute("subject", subject);
        return "subject/edit";
    }

    @PostMapping("/admin/subject/edit")
    public String saveEditSubject(Model model, Subject subject) {
        subjectService.save(subject);
        model.addAttribute("message", "Saved");
        return "subject/edit";
    }

    @GetMapping("/admin/subject/delete/{id}")
    public String deleteSubject(@PathVariable Long id, Model model) {
        Subject subject = subjectService.findById(id);
        model.addAttribute("subject", subject);
        return "subject/delete";
    }

    @PostMapping("/admin/subject/delete")
    public String saveDeleteSubject(@ModelAttribute("subject") Subject subject, RedirectAttributes redirectAttributes) {
        if(markService.findAllBySubject(subject).isEmpty()){
            subjectService.delete(subject.getId());
            return "redirect:/admin/subjects";
        } else {
            redirectAttributes.addFlashAttribute("message", "please delete all marks of subject");
            return "redirect:/admin/subject/delete/"+subject.getId();
        }
    }
}
