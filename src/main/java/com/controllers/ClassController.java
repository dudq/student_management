package com.controllers;

import com.models.Class;
import com.models.Student;
import com.services.ClassService;
import com.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class ClassController {
    @Autowired
    private ClassService classService;

    @Autowired
    private StudentService studentService;


    @GetMapping("/admin/class/classes")
    public ModelAndView listClasses(@RequestParam("key_word") Optional<String> key_word, @PageableDefault(size = 5, sort = "name") Pageable pageable) {
        Page<Class> classes;
        if (key_word.isPresent()) {
            classes = classService.findAllByNameContaining(key_word.get(), pageable);
        } else {
            classes = classService.findAll(pageable);
        }

        ModelAndView modelAndView = new ModelAndView("/class/list");
        modelAndView.addObject("classes", classes);
        return modelAndView;
    }

    @GetMapping("/admin/class/create-class")
    public ModelAndView showCreateForm(){
        ModelAndView modelAndView = new ModelAndView("/class/create");
        modelAndView.addObject("class", new Class());
        return modelAndView;
    }

    @PostMapping("/admin/class/create-class")
    public ModelAndView saveClass(@Valid @ModelAttribute("class") Class clazz, BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()){
            ModelAndView modelAndView = new ModelAndView("/class/create");
            return modelAndView;
        }
        if (classService.isExistedClass(clazz)){
            ModelAndView modelAndView = new ModelAndView("/class/create");
            modelAndView.addObject("class", clazz);
            modelAndView.addObject("message", "Name duplicate");
            return modelAndView;
        } else {
            classService.save(clazz);
            ModelAndView modelAndView = new ModelAndView("/class/create");
            modelAndView.addObject("class", new Class());
            modelAndView.addObject("message", "Added successfully");
            return modelAndView;
        }
    }

    @GetMapping("/admin/class/edit-class/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Class clazz = classService.findById(id);
        if (clazz != null) {
            ModelAndView modelAndView = new ModelAndView("/class/edit");
            modelAndView.addObject("class", clazz);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("error.404");
            return modelAndView;
        }
    }

    @PostMapping("/admin/class/edit-class")
    public ModelAndView updateClass(@Valid @ModelAttribute("class") Class clazz, BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()){
            ModelAndView modelAndView = new ModelAndView("/class/edit");
            return modelAndView;
        }
        if (classService.isExistedClass(clazz)) {
            ModelAndView modelAndView = new ModelAndView("/class/edit");
            modelAndView.addObject("class", clazz);
            modelAndView.addObject("message", "Name duplicate");
            return modelAndView;
        } else {
            classService.save(clazz);
            ModelAndView modelAndView = new ModelAndView("/class/edit");
            modelAndView.addObject("class", clazz);
            modelAndView.addObject("message", "Updated successfully");
            return modelAndView;
        }
    }

    @GetMapping("/admin/class/delete-class/{id}")
    public ModelAndView showDeleteForm(@PathVariable Long id){
        Class clazz = classService.findById(id);
        if(clazz != null) {
            ModelAndView modelAndView = new ModelAndView("/class/delete");
            modelAndView.addObject("class", clazz);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/admin/class/delete-class")
    public ModelAndView deleteClass(@ModelAttribute("class") Class clazz, RedirectAttributes redirectAttributes) {
        if (studentService.findAllByClazz(clazz).isEmpty()) {
            classService.delete(clazz.getId());
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/class/classes");
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/class/delete-class/" + clazz.getId());
            redirectAttributes.addFlashAttribute("message", "Please delete all student in class");
            return modelAndView;
        }

    }

    @GetMapping("/admin/class/view/{id}")
    public ModelAndView getAllStudentByClass(@PathVariable("id") Long id) {
        Class clazz = classService.findById(id);
        if (clazz != null) {
            List<Student> students = studentService.findAllByClazz(clazz);
            ModelAndView modelAndView = new ModelAndView("/class/view");
            modelAndView.addObject("class", clazz);
            modelAndView.addObject("students", students);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
}
