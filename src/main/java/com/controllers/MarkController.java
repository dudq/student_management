package com.controllers;

import com.formatter.MarkValidator;
import com.models.Mark;
import com.models.Student;
import com.models.Subject;
import com.services.MarkService;
import com.services.StudentService;
import com.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
public class MarkController {


    @Autowired
    private SubjectService subjectService;

    @ModelAttribute("subjects")
    public Iterable<Subject> subjects(){
        return subjectService.findAllBy();
    }

    @Autowired
    private StudentService studentService;

    @ModelAttribute("students")
    public Iterable<Student> students(){
        return studentService.findAllBy();
    }

    @Autowired
    private MarkService markService;

    @GetMapping("/admin/mark")
    public ModelAndView showMarkList(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("/mark/list");
        modelAndView.addObject("mark", markService.findAll(pageable));
        return modelAndView;
    }

    @GetMapping("/admin/mark/create")
    public ModelAndView showCreateMarkForm() {
        ModelAndView modelAndView = new ModelAndView("/mark/create");
        modelAndView.addObject("mark", new Mark());
        return modelAndView;
    }

    @PostMapping("/admin/mark/create")
    public ModelAndView saveNewMark(@Validated @ModelAttribute("mark") Mark mark, BindingResult bindingResult) {
        new MarkValidator().validate(mark, bindingResult);
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/mark/create");
            modelAndView.addObject("mark", mark);
            return modelAndView;
        }
        markService.save(mark);

        ModelAndView modelAndView = new ModelAndView("/mark/create");
        modelAndView.addObject("mark", new Mark());
        modelAndView.addObject("message", "New mark created successfully");
        return modelAndView;
    }

    @GetMapping("/admin/mark/edit/{id}")
    public ModelAndView showEditMarkForm(@PathVariable("id") Long id) {
        Mark mark = markService.findById(id);

        if (mark != null) {
            ModelAndView modelAndView = new ModelAndView("/mark/edit");
            modelAndView.addObject("mark", mark);
            return modelAndView;
        } else {
            return new ModelAndView("error");
        }
    }

    @PostMapping("/admin/mark/edit/{id}")
    public ModelAndView saveEditedMark(@Validated @ModelAttribute("mark") Mark mark, BindingResult bindingResult,@PathVariable("id") Long id) {
        new MarkValidator().validate(mark, bindingResult);
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/mark/edit");
            Mark newMark = markService.findById(id);
            modelAndView.addObject("mark", newMark);
            modelAndView.addObject("message","Invalid input");
            return modelAndView;
        }
        markService.save(mark);

        ModelAndView modelAndView = new ModelAndView("/mark/edit");
        modelAndView.addObject("mark", mark);
        modelAndView.addObject("message", "Mark updated successfully");
        return modelAndView;
    }

    @GetMapping("/admin/mark/delete/{id}")
    public ModelAndView showDeleteMarkForm(@PathVariable("id") Long id) {
        Mark mark = markService.findById(id);

        if (mark != null) {
            ModelAndView modelAndView = new ModelAndView("/mark/delete");
            modelAndView.addObject("mark", mark);
            return modelAndView;
        } else {
            return new ModelAndView("error");
        }
    }

    @PostMapping("/admin/mark/delete")
    public ModelAndView deletedPhone(@ModelAttribute("mark") Mark mark, Pageable pageable) {
        markService.delete(mark.getId());

        ModelAndView modelAndView = new ModelAndView("redirect:/admin/mark");

        return modelAndView;
    }

    @GetMapping("/admin/mark/searchByStudent")
    public String getMarkByStudent(@RequestParam("studentId") Long studentId, Model model){
        List<Mark> marks;
        if(studentId == -1) {
            return "redirect:/admin/mark";
        } else {
            Student student = studentService.findById(studentId);
            marks = markService.findAllByStudent(student);
        }
        model.addAttribute("mark", marks);
        model.addAttribute("studentId", studentId);
        return "mark/list";
    }

    @GetMapping("/admin/mark/sort")
    public String markSort(Model model, @RequestParam Optional<String> sortBy){
        if (sortBy.get().equals("-1")){
            return "redirect:/admin/mark";
        }
        String sort = sortBy.get();
        Pageable pageable = new PageRequest(0,100,new Sort(sort));
        Page<Mark> marks = markService.findAll(pageable);
        model.addAttribute("mark", marks);
        return "/mark/list";
    }

}
