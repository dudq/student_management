package com.controllers;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    @GetMapping("/my-login")
    public ModelAndView getLoginForm() {
        SecurityContext context = SecurityContextHolder.getContext();
        return new ModelAndView("login");
    }

}
