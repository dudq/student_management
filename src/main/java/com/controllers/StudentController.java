package com.controllers;

import com.models.Class;
import com.models.Mark;
import com.models.Student;
import com.services.ClassService;
import com.services.MarkService;
import com.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller

public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    private ClassService classService;

    @Autowired
    private MarkService markService;

    @ModelAttribute("classes")
    public Iterable<Class> classes() {
        return classService.findAll();
    }

    @GetMapping("/admin/students")
    public String studentList(Model model, @PageableDefault(size = 10, sort = "name") Pageable pageable,
                              @RequestParam("key_word") Optional<String> key_word) {
        Page<Student> students;
        if (key_word.isPresent()) {
            students = studentService.findByNameContaining(key_word.get(), pageable);
            model.addAttribute("key_word", key_word.get());
        } else {
            students = studentService.findAll(pageable);
        }
        model.addAttribute("students", students);
        return "student/list";
    }

    @GetMapping("/admin/create-student")
    public String createStudent(Model model) {
        model.addAttribute("student", new Student());
        return "student/create";
    }

    @PostMapping("/admin/create-student")
    public String saveNewStudent(@Validated @ModelAttribute("student") Student student, BindingResult bindingResult, Model model) {
        new Student().validate(student, bindingResult);
        if (!bindingResult.hasFieldErrors()) {
            studentService.save(student);
            model.addAttribute("message", "Added new student");
            model.addAttribute("student", new Student());
        }
        return "student/create";
    }

    @GetMapping("/admin/student/edit/{id}")
    public String editStudent(@PathVariable Long id, Model model) {
        Student student = studentService.findById(id);
        model.addAttribute("student", student);
        return "student/edit";
    }

    @PostMapping("/admin/student/edit")
    public String saveEditStudent(Model model, Student student) {
        studentService.save(student);
        model.addAttribute("message", "Saved");
        return "student/edit";
    }

    @GetMapping("/admin/student/delete/{id}")
    public String deleteStudent(@PathVariable Long id, Model model) {
        Student student = studentService.findById(id);
        model.addAttribute("student", student);
        return "student/delete";
    }

    @PostMapping("/admin/student/delete")
    public String saveDeleteStudent(@ModelAttribute("student") Student student, RedirectAttributes redirectAttributes) {
        if (markService.findAllByStudent(student).isEmpty()){
            studentService.delete(student.getId());
            return "redirect:/admin/students";
        } else {
            redirectAttributes.addFlashAttribute("message", "please delete all marks of student");
            return "redirect:/admin/student/delete/" + student.getId();
        }
    }

    @GetMapping("/admin/student/searchByClass")
    public String getStudentByClass(@RequestParam("classId") Long classId, Pageable pageable, Model model) {
        Page<Student> students;
        if (classId == -1) {
            return "redirect:/admin/students";
        } else {
            Class clazz = classService.findById(classId);
            students = studentService.findAllByClazzOrderByName(clazz, pageable);
        }
        model.addAttribute("students", students);
        model.addAttribute("classId", classId);
        return "student/list";

    }

    @GetMapping("/admin/student/view/{id}")
    public String getStudentDetail(@PathVariable("id") Long id, Model model) {
        Student student = studentService.findById(id);
        if (student != null) {
            List<Mark> marks = markService.findAllByStudent(student);
            model.addAttribute("marks", marks);
            model.addAttribute("student", student);
            return "/student/view";
        } else {
            return "error.404";
        }
    }

    @GetMapping("/admin/students/sort")
    public String studentSort(Model model, @RequestParam Optional<String> sortBy) {
        if (sortBy.get().equals("-1")) {
            return "redirect:/admin/students";
        }
        String sort = sortBy.get();
        Pageable pageable = new PageRequest(0, 100, new Sort(sort));
        Page<Student> students = studentService.findAll(pageable);
        model.addAttribute("students", students);
        return "/student/list";
    }
}
