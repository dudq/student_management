package com.services;

import com.models.Mark;
import com.models.Student;
import com.models.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MarkService {
    Page<Mark> findAll(Pageable pageable);

    Mark findById(Long id);

    void save(Mark mark);

    void delete(Long id);

    List<Mark> findAllByStudent(Student student);

    List<Mark> findAllBySubject(Subject subject);
}
