package com.services.impl;

import com.models.Class;
import com.repositories.ClassRepository;
import com.services.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClassServiceImpl implements ClassService {

    @Autowired
    private ClassRepository classRepository;

//    @Autowired
//    private StudentService studentService;

    @Override
    public Page<Class> findAll(Pageable pageable) {
        return classRepository.findAll(pageable);
    }


    @Override
    public Class findById(Long id) {
        return classRepository.findOne(id);
    }

    @Override
    public void save(Class clazz) {
        classRepository.save(clazz);
    }

    @Override
    public void delete(Long id) {
        classRepository.delete(id);
    }

    @Override
    public Page<Class> findAllByNameContaining(String name, Pageable pageable) {
        return classRepository.findAllByNameContaining(name, pageable);
    }

    @Override
    public Iterable<Class> findAll() {
        return classRepository.findAll();
    }

    @Override
    public boolean isExistedClass(Class clazz) {
        List<Class> existedClass = classRepository.findByName(clazz.getName());
        if (!existedClass.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
