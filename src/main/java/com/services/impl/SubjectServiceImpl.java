package com.services.impl;

import com.models.Subject;
import com.repositories.SubjectRepository;
import com.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class SubjectServiceImpl implements SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Page<Subject> findAll(Pageable pageable) {

        return subjectRepository.findAll(pageable);
    }

    @Override
    public Subject findById(Long id) {
        return subjectRepository.findOne(id);
    }


    @Override
    public void save(Subject subject) {
        subjectRepository.save(subject);
    }

    @Override
    public void delete(Long id) {
        subjectRepository.delete(id);
    }

    @Override
    public Iterable<Subject> findAllBy() {
        return subjectRepository.findAllBy();
    }
}

