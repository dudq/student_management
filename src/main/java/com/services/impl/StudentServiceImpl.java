package com.services.impl;

import com.models.Class;
import com.models.Student;
import com.repositories.StudentRepository;
import com.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Page<Student> findAll(Pageable pageable) {

        return studentRepository.findAll(pageable);
    }

    @Override
    public Student findById(Long id) {
        return studentRepository.findOne(id);
    }


    @Override
    public void save(Student student) {
        studentRepository.save(student);
    }

    @Override
    public void delete(Long id) {
        studentRepository.delete(id);
    }

    @Override
    public Page<Student> findAllByClazzOrderByName(Class clazz, Pageable pageable) {
        return studentRepository.findAllByClazzOrderByName(clazz, pageable);
    }

    @Override
    public List<Student> findAllByClazz(Class clazz) {
        return studentRepository.findAllByClazz(clazz);
    }

    @Override
    public Page<Student> findByNameContaining(String key_word, Pageable pageable) {
        return studentRepository.findByNameContaining(key_word, pageable);
    }

    @Override
    public Iterable<Student> findAllBy() {
        return studentRepository.findAllBy();
    }

}