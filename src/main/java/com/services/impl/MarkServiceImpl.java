package com.services.impl;

import com.models.Mark;
import com.models.Student;
import com.models.Subject;
import com.repositories.MarkRepository;
import com.services.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class MarkServiceImpl implements MarkService {

    @Autowired
    MarkRepository markRepository;

    @Override
    public Page<Mark> findAll(Pageable pageable) {
        return markRepository.findAll(pageable);
    }

    @Override
    public Mark findById(Long id) {
        return markRepository.findOne(id);
    }

    @Override
    public void save(Mark mark) {
        markRepository.save(mark);
    }

    @Override
    public void delete(Long id) {
        markRepository.delete(id);
    }

    @Override
    public List<Mark> findAllByStudent(Student student) {
        return markRepository.findAllByStudent(student);
    }

    @Override
    public List<Mark> findAllBySubject(Subject subject) {
        return markRepository.findAllBySubject(subject);
    }

}
