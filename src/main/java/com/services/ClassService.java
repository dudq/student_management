package com.services;

import com.models.Class;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClassService {

    Page<Class> findAll (Pageable pageable);

    Class findById(Long id);

    void save(Class clazz);

    void delete(Long id);

    Page<Class> findAllByNameContaining(String name, Pageable pageable);

    Iterable<Class> findAll();

    boolean isExistedClass(Class clazz);
}
