package com.services;

import com.models.Class;
import com.models.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface StudentService {
    Page<Student> findAll(Pageable pageable);

    Student findById(Long id);

    void save(Student student);

    void delete(Long id);

    Page<Student> findAllByClazzOrderByName(Class clazz, Pageable pageable);

    List<Student> findAllByClazz(Class clazz);

    Page<Student> findByNameContaining(String key_word, Pageable pageable);

    Iterable<Student> findAllBy();

}
