package com.services;

import com.models.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SubjectService {
    Page<Subject> findAll(Pageable pageable);

    Subject findById(Long id);

    void save(Subject subject);

    void delete(Long id);

    Iterable<Subject> findAllBy();

}

