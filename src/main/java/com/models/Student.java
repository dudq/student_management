package com.models;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "students")
@Component
public class Student implements Validator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @NotEmpty(message = "Name must not be empty")
    @NotBlank(message = "Invalid name")
    private String name;

    private Date dateOfBirth;

    @Email
    @NotNull
    private String mail;

    private String phoneNumber;

    @ManyToOne
    @JoinColumn(name = "class_id")
    @NotNull
    private Class clazz;

    @OneToMany(targetEntity = Mark.class)
    private List<Mark> marks;

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public Student() {
    }

    public Student(String name, Date dateOfBirth, String mail, String phoneNumber) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.mail = mail;
        this.phoneNumber = phoneNumber;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean supports(java.lang.Class<?> clazz) {
        return Student.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Student student = (Student) target;
        String number = student.getPhoneNumber();
        ValidationUtils.rejectIfEmpty(errors, "dateOfBirth", "date.empty", "Invalid date");
        ValidationUtils.rejectIfEmpty(errors, "phoneNumber", "phoneNumber.empty", "Phone number must not empty");
        if (number.length() > 11 || number.length() < 10) {
            errors.rejectValue("phoneNumber", "number.length", "length form 10 to 11.");
        }
        if (!number.startsWith("0")) {
            errors.rejectValue("phoneNumber", "number.startsWith", "phone number start with 0.");
        }
        if (!number.matches("(^$|[0-9]*$)")) {
            errors.rejectValue("phoneNumber", "number.matches", "phone number only include number.");
        }
    }

}
