package com.repositories;

import com.models.Class;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ClassRepository extends PagingAndSortingRepository<Class, Long> {
    Page<Class> findAllByNameContaining(String name, Pageable pageable);

    List<Class> findByName(String name);
}
