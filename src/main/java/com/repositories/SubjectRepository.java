package com.repositories;

import com.models.Subject;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long> {
        Iterable<Subject> findAllBy();

}
