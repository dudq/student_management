package com.repositories;

import com.models.Class;
import com.models.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
    Page<Student> findAllByClazzOrderByName(Class clazz, Pageable pageable);

    List<Student> findAllByClazz(Class clazz);

    Page<Student> findByNameContaining(String key_word, Pageable pageable);

    Iterable<Student> findAllBy();
}
