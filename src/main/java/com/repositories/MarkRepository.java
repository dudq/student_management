package com.repositories;

import com.models.Mark;
import com.models.Student;
import com.models.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MarkRepository extends PagingAndSortingRepository<Mark, Long> {
    Page<Mark> findAllByOrderByMark(Pageable pageable);

    List<Mark> findAllByStudent(Student student);

    List<Mark> findAllBySubject(Subject subject);

}
